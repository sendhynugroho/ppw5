from django.db import models
from django import forms
from django.core.validators import *
from django.core.exceptions import ValidationError

length_validation = MinLengthValidator(8)

class Jadwal(models.Model):
    nama = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    tanggal = models.DateField()

class Signup(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=50)