from django.contrib import admin
from .models import Signup
from .forms import PostForm

class PostForm(admin.ModelAdmin):
    list_display = ('name', 'email', 'password') 

admin.site.register(Signup, PostForm)
