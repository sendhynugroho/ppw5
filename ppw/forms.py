from django import forms
from . import models
from .models import Signup
from django.core.validators import *

length_validation = MinLengthValidator(8)

class BuatJadwal(forms.ModelForm):
    class Meta:
        model = models.Jadwal
        fields = ['nama','kategori','tempat','tanggal']

class PostForm(forms.ModelForm):
    class Meta:
        model = Signup
        fields = ['name', 'email', 'password']
        widgets = {

            'name': forms.TextInput(
                attrs={'class': 'form-control', 'id':'name_form', 'placeholder':'Input your name', 'maxlength': 50}),

            'email': forms.TextInput(
                attrs={'type':'email','class': 'form-control', 'id':'email_form', 'placeholder':'Input your email', 'maxlength': 50}),

            'password': forms.TextInput(
                attrs={ 'type':'password' ,'class': 'form-control', 'id':'password_form', 'placeholder':'Minimum of 8 characters', 'minlength':8, 'maxlength': 50}),
        }