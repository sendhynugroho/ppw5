from django.urls import path
from . import views

#url for app
urlpatterns = [
    path('', views.index, name='index'),
    path('contact/', views.contact, name='contact')
]
