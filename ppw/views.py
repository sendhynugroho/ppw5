from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse , HttpResponse
from . import forms
from . import models
from .models import Jadwal
from .forms import PostForm
from .models import Signup
import requests
import json
import re

# Create your views here.
response = {'author' : 'Sendhy Nugroho'}

def index(request):
    return render(request, 'ppw/index.html')

def contact(request):
    return render(request, 'ppw/contact.html')

