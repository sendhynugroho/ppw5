// ajax function for create user POST method
function subscribe() {
    
    // alert("Failed One");
    $.ajax({
        url: "/subscribe/",
        type: "POST",
        data: {
            email: $('#email_form').val(),
            name: $('#name_form').val(),
            password: $('#password_form').val()
        },

        success: function (json) {
            // console.log(json);
            $('#contact').val(''); // empty form
            $('#msg_response').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>Account is created</a></div>")
        },

        error: function (xhr, errmsg, err) {
            $('#msg_response').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>Error message &times;</a></div>");
        },
    
    });
};

function validate_user() {
    $.ajax({
        url: "/validate/",
        type: "POST",
        data: {

            email: $('#email_form').val(),
            name: $('#name_form').val(),
            password: $('#password_form').val()
            
        },

        success: function (response) {

            // console.log(response);
            $('#msg_validate').html("<div class='alert-box alert radius' data-alert><a href='#' class='close'>" + response.message + "</a></div>")

            if (response.message == "All fields are valid"){

                document.getElementById('subscribe_button').disabled = false;
            }

            else {

                document.getElementById('subscribe_button').disabled = true;
            }

        },

        // debugging
        error: function (errmsg) {

            console.log(errmsg + "ERROR DETECTED");
        }

    });
};

$(document).ready(function () {
    var x_timer;
    $("#email_form").keyup(function (e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 50);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#name_form").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 50);
        //document.getElementById('subscribe_button').disabled = false;
    });
});

$(document).ready(function () {
    var x_timer;
    $("#password_form").keyup(function (e) {
        clearTimeout(x_timer);
        var name = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 50);
    });
});