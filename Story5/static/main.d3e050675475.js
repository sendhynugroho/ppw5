function subscribe() {
    
    $.ajax({
        url: "/subscribe/",
        type: "POST",
        data: {
            email: $('#email_form').val(),
            name: $('#name_form').val(),
            password: $('#password_form').val()
        },

        success: function (json) {
            // console.log(json);
            $('#subscribe_form').val(''); // empty form
            $('#msg_response').html("<div class='alert alert-success'><strong>Success!</strong> Account has been created.</div>")
        },

        error: function (xhr, errmsg, err) {
            $('#msg_response').html("<div class='alert alert-danger'><strong>Error!</strong>Something has wrong.</div>");
        },
    
    });
};

function validate_user() {
    $.ajax({
        url: "/validate/",
        type: "POST",
        data: {

            email: $('#email_form').val(),
            name: $('#name_form').val(),
            password: $('#password_form').val()
            
        },

        success: function (response) {

            if (response.message == "All fields are valid"){

                document.getElementById('subscribe_button').disabled = false;
                $('#msg_validate').html("<p style='color:green'>"+ response.message + "</p>")

            }

            else {

                document.getElementById('subscribe_button').disabled = true;
                $('#msg_validate').html("<p style='color:red'>"+ response.message + "</p>")
            }

        },

        // debugging
        error: function (errmsg) {

            console.log(errmsg + "ERROR DETECTED");
        }

    });
};

$(document).ready(function () {
    var x_timer;
    $("#name_form").keyup(function (e) {
        clearTimeout(x_timer);
        var password = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);

    });
});

$(document).ready(function () {
    var x_timer;
    $("#email_form").keyup(function (e) {
        clearTimeout(x_timer);
        var email = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);
    });
});

$(document).ready(function () {
    var x_timer;
    $("#password_form").keyup(function (e) {
        clearTimeout(x_timer);
        var nama = $(this).val();
        x_timer = setTimeout(function () {
            validate_user();
        }, 10);
    });
});